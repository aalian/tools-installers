#!/bin/bash

cd /opt

sudo mv 'kafka_2.12-2.3.1' kafka

sudo chown -R kafka:kafka kafka

sudo chown -R kafka:kafka kafka/*


cd /opt/kafka/config


#sed -i '/port =/d' server.properties
#sed -i '/node.name:/d' server.properties
#sed -i '/discovery.seed_hosts:/d' server.properties




echo ' ' >> server.properties

echo 'delete.topic.enable=true' >> server.properties
echo 'default.replication.factor=3' >> server.properties
echo 'replica.lag.time.max.ms=15000' >> server.properties
echo 'min.insync.replicas=2' >> server.properties
echo 'num.replica.fetchers=3' >> server.properties
echo 'auto.create.topics.enable=true' >> server.properties
echo 'log.cleanup.policy=delete' >> server.properties
echo 'advertised.host.name =' >> server.properties
echo 'port =' >> server.properties


echo 'advertised.host.name node ip? (eg: 10.0.0.10)'

read ip

sed -i -e "s/\(advertised.host.name =\).*/\1$ip/" /opt/kafka/config/server.properties




echo 'What is your nodes ips (zookeeper connect)? (eg: 10.0.0.10:2181,10.0.0.11:2181,...)'

read ip

sed -i -e "s/\(zookeeper.connect=\).*/\1$ip/" /opt/kafka/config/server.properties



echo 'What is your node port? (ex:9092)'

read port

sed -i -e "s/\(port =\).*/\1$port/" /opt/kafka/config/server.properties



echo 'What is your zookeeper node id? (eg: 1,2,3)'

read id

sed -i -e "s/\(broker.id=\).*/\1$id/" /opt/kafka/config/server.properties



cp /etc/zookeeper/conf/myid /data/zookeeper
sudo chown zookeeper:zookeeper /data/zookeeper/myid



systemctl daemon-reload

sleep 1s


systemctl restart zookeeper


sleep 1s

systemctl restart kafka


