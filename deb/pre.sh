#!/bin/bash

sudo useradd kafka 

cd /etc/systemd/system

sudo touch kafka.service

echo "[Unit]
Requires=zookeeper.service
After=zookeeper.service

[Service]
Type=simple
User=kafka
ExecStart=/bin/sh -c '/opt/kafka/bin/kafka-server-start.sh /opt/kafka/config/server.properties > /opt/kafka/kafka.log 2>&1'
ExecStop=/opt/kafka/bin/kafka-server-stop.sh
Restart=on-abnormal

[Install]
WantedBy=multi-user.target" >> kafka.service


cd /etc/zookeeper/conf/

echo 'What is your first zookeeper server ip? (eg: 10.0.0.10)'

read ip

echo ' ' >> /etc/zookeeper/conf/zoo.cfg

echo 'server.1='$ip':2888:3888' >>  /etc/zookeeper/conf/zoo.cfg

echo 'What is your second zookeeper server ip? (eg: 10.0.0.10)'

read ip

echo ' ' >> /etc/zookeeper/conf/zoo.cfg

echo 'server.2='$ip':2888:3888' >>  /etc/zookeeper/conf/zoo.cfg

echo 'What is your third server ip? (eg: 10.0.0.10)'

read ip

echo ' ' >> /etc/zookeeper/conf/zoo.cfg

echo 'server.3='$ip':2888:3888' >>  /etc/zookeeper/conf/zoo.cfg

echo ' ' >> /etc/zookeeper/conf/zoo.cfg

echo 'autopurge.snapRetainCount=3' >> /etc/zookeeper/conf/zoo.cfg

echo 'autopurge.purgeInterval=1' >> /etc/zookeeper/conf/zoo.cfg

touch myid

echo 'What is this node id zookeeper server ip? (eg: 1,2,3)'

read id

echo $id >> /etc/zookeeper/conf/myid

