#!/bin/bash

adduser --disabled-password zookeeper
sudo usermod --shell /bin/bash zookeeper
usermod -aG sudo zookeeper
sudo mkdir -p /data/zookeeper
sudo chown zookeeper:zookeeper /data/zookeeper
sudo tar -xvf zookeeper-3.4.8.tar.gz -C /etc/
cd /etc
sudo chown zookeeper:zookeeper -R  zookeeper-3.4.8/
sudo ln -s zookeeper-3.4.8 zookeeper
sudo chown -h zookeeper:zookeeper zookeeper

cd /etc/zookeeper/conf/

touch zoo.cfg

echo "tickTime=2000
dataDir=/data/zookeeper
clientPort=2181
maxClientCnxns=60
initLimit=10
syncLimit=5
" >> zoo.cfg

cd /etc/systemd/system

touch zookeeper.service

echo "[Unit]
Description=Zookeeper Daemon
Documentation=http://zookeeper.apache.org
Requires=network.target
After=network.target

[Service]
Type=forking
WorkingDirectory=/etc/zookeeper
Environment=ZOO_LOG_DIR=/data/zookeeper
User=zookeeper
Group=zookeeper
ExecStart=/etc/zookeeper/bin/zkServer.sh start /etc/zookeeper/conf/zoo.cfg
ExecStop=/etc/zookeeper/bin/zkServer.sh stop /etc/zookeeper/conf/zoo.cfg
ExecReload=/etc/zookeeper/bin/zkServer.sh restart /etc/zookeeper/conf/zoo.cfg
TimeoutSec=30
Restart=on-failure

[Install]
WantedBy=default.target" >> zookeeper.service

