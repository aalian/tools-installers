#/bin/bash

cd /home/ec2-user/kafka-installation-RPM

rpm -iavh jdk-8u231-linux-x64.rpm

rpm -iavh zookeeper-3.4.9-1.el7.noarch.rpm

rpm -iavh kafka-1.0-1.x86_64.rpm


sleep 2s

./postinstallation-configuration.sh

