#/bin/bash


echo '####################################################'
echo '############## Zookeper Configuration ##############'
echo '####################################################'


cd /etc/zookeeper/conf

echo 'What is your first zookeeper server ip? (eg: 10.0.0.10)'

read ip

echo ' ' >> /etc/zookeeper/conf/zoo_sample.cfg

echo 'server.1='$ip':2888:3888' >> /etc/zookeeper/conf/zoo_sample.cfg

echo 'What is your second zookeeper server ip? (eg: 10.0.0.10)'

read ip

echo ' ' >> /etc/zookeeper/conf/zoo_sample.cfg

echo 'server.2='$ip':2888:3888' >>  /etc/zookeeper/conf/zoo_sample.cfg

echo 'What is your third server ip? (eg: 10.0.0.10)'

read ip

echo ' ' >> /etc/zookeeper/conf/zoo_sample.cfg

echo 'server.3='$ip':2888:3888' >>  /etc/zookeeper/conf/zoo_sample.cfg

echo ' ' >> /etc/zookeeper/conf/zoo_sample.cfg

echo 'autopurge.snapRetainCount=3' >> /etc/zookeeper/conf/zoo_sample.cfg

echo 'autopurge.purgeInterval=1' >> /etc/zookeeper/conf/zoo_sample.cfg


touch myid

echo 'What is this node id zookeeper server ip? (eg: 1,2,3)'

read id

echo $id >> /etc/zookeeper/conf/myid




echo '####################################################'
echo '############## Kafka-Configuration #################'
echo '####################################################'




cd /opt/kafka/config/

echo 'What is your node ip? (eg: 10.0.0.10)'

read ip

sed -i -e "s/\(advertised.host.name = \).*/\1$ip/" /opt/kafka/config/server.properties



echo 'What is your nodes ips? (eg: 10.0.0.10:2181,10.0.0.11:2181,...)'

read ip

sed -i -e "s/\(zookeeper.connect=\).*/\1$ip/" /opt/kafka/config/server.properties



echo 'What is your node port?'

read port

sed -i -e "s/\(port = \).*/\1$port/" /opt/kafka/config/server.properties



echo 'What is your zookeeper node id? (eg: 1,2,3)'

read id

sed -i -e "s/\(broker.id=\).*/\1$id/" /opt/kafka/config/server.properties


export JAVA_HOME=/usr/java/jdk1.8.0_231-amd64
sudo update-alternatives --install /usr/bin/java java ${JAVA_HOME%*/}/bin/java 20000
sudo update-alternatives --install /usr/bin/javac javac ${JAVA_HOME%*/}/bin/javac 20000
sudo update-alternatives --set java /usr/java/jdk1.8.0_231-amd64/bin/java
sudo update-alternatives --set javac /usr/java/jdk1.8.0_231-amd64/bin/javac


systemctl daemon-reload

sleep 1s


systemctl restart zookeeper


sleep 1s

systemctl restart kafka

