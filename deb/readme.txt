installation instructions:

copy the following file to the targeted devices that you want to install full kafka cluster on them:

1. java_1.0_amd64.deb
2. zookeeper-3.4.8.tar.gz
3. kafka_1.0_amd64.deb
4. zookeeperinstall.sh

start by installing Java 1.8.u231 by entering the following command:

---> dpkg -ia java_1.0_amd64.deb

#NOTE!! check the installation if its done successfully by entering the following command:

---> java -version 

run the following script to install zookeeper by entering the following command:

---> ./zookeeperinstall.sh

install kafka by entering the following command:

---> dpkg -ia kafka_1.0_amd64.deb

#NOTE!! during the installation of kafka you'll be asked to enter the needed configuration.




