#!/bin/bash

cd /etc/systemd/system

touch zookeeper.service

echo "[Unit]
Description=Zookeeper Daemon
Documentation=http://zookeeper.apache.org
Requires=network.target
After=network.target

[Service]    
Type=forking
WorkingDirectory=/etc/zookeeper
User=zookeeper
Group=zookeeper
ExecStart=/etc/zookeeper/bin/zkServer.sh start /etc/zookeeper/conf/zoo.cfg
ExecStop=/etc/zookeeper/bin/zkServer.sh stop /etc/zookeeper/conf/zoo.cfg
ExecReload=/etc/zookeeper/bin/zkServer.sh restart /etc/zookeeper/conf/zoo.cfg
TimeoutSec=30
Restart=on-failure

[Install]
WantedBy=default.target
" >> zookeeper.service

cd /home/ubuntu

sudo adduser --disabled-password zookeeper
sudo usermod --shell /bin/bash zookeeper
usermod -aG sudo zookeeper
sudo mkdir -p /data/zookeeper
sudo mkdir -p /etc/
sudo chown zookeeper:zookeeper /data/zookeeper
tar xzvf zookeeper-3.4.8.tar.gz -C /etc/
cd /etc
sudo chown zookeeper:zookeeper -R  zookeeper-3.4.8
sudo ln -s zookeeper-3.4.8 zookeeper
sudo chown -h zookeeper:zookeeper zookeeper

